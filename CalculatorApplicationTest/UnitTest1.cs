using CalculatorApplication.Controllers;

namespace CalculatorApplicationTest
{
    public class UnitTest1
    {

        [Fact]
        public void TestAdd()
        {
            HomeController _calculator = new HomeController();

            Assert.Equal(-1, _calculator.Add(2, -3));
            Assert.Equal(5, _calculator.Add(2, 3));
        }

        [Fact]
        public void TestSubtract()
        {
            HomeController _calculator = new HomeController();
            Assert.Equal(2, _calculator.Subtract(5, 3));
        }

        [Fact]
        public void TestMultiply()
        {
            HomeController _calculator = new HomeController();
            Assert.Equal(15, _calculator.Multiply(3, 5));
        }

        [Fact]
        public void TestDivide()
        {
            HomeController _calculator = new HomeController();
            Assert.Equal(2, _calculator.Divide(6, 3));
        }

        [Fact]
        public void TestDivideByZero()
        {
            HomeController _calculator = new HomeController();
            Assert.Throws<DivideByZeroException>(() => _calculator.Divide(6, 0));
        }
    }
}